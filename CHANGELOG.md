# Changelog

All notable changes to this project will be documented in this file.

## [Ideas]

- Low priority, but could add a fancy report like lazyrecon does.

## [Unreleased]

- None.

## [0.0.4] - 2020-02-28

### Added

- None.

### Changed

- Changed minor bugs.

### Removed

- None.

## [0.0.3] - 2020-02-28

### Added

- None.

### Changed

- Changed minor bugs.

### Removed

- None.

## [0.0.2] - 2020-02-28

### Added

- Added README.

### Changed

- Changed minor bugs.

### Removed

- None.

## [0.0.1] - 2020-02-27

### Added

- Initial release.

## Changed

- None.

## Removed

- None.
