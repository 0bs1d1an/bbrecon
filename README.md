[![pipeline status](https://gitlab.com/0bs1d1an/bbrecon/badges/master/pipeline.svg)](https://gitlab.com/0bs1d1an/bbrecon/commits/master)

# Bug Bounty Recon (bbrecon)

This tool is inspired by [@nahamsec's lazyrecon](https://github.com/nahamsec/lazyrecon).
It aims to be a refactor of lazyrecon, with some more features, such as an nmap port scan of all harvested subdomains.

## Rationale

The main idea was to package lazyrecon for [Pentoo Linux](http://github.com/pentoo/pentoo-overlay), but I found myself making so many exceptions in the code for it to be packaged that I started wondering if I should first clean up the code and submit several big pull requests.
That would take a very long time, and then I was skeptical if I could do it in such a way that lazyrecon would still work for people that use it in combination with [@nahamsec's bbht](https://github.com/nahamsec/bbht), and package it for Pentoo.
After weighing my options I decided to refactor it myself from scratch, with a Gentoo-like terminal look, and give proper credits to nahamsec under the logo, and here in the README.

## Dependencies

1. aquatone (net-analyzer/aquatone);
1. awk (virtual/awk);
1. bash (app-shells/bash);
1. bind-tools (net-dns/bind-tools);
1. chromium (www-client/chromium);
1. coreutils (sys-apps/coreutils);
1. dirsearch (net-analyzer/dirsearch);
1. findutils (sys-apps/findutils);
1. grep (sys-apps/grep);
1. httprobe (net-misc/httprobe);
1. jq (app-misc/jq);
1. massdns (net-analyzer/massdns);
1. ncurses (sys-libs/ncurses);
1. nmap (net-analyzer/nmap)
1. seclists (app-dicts/seclists);
1. sed (sys-apps/sed);
1. sublist3r (net-analyzer/sublist3r);
1. unfurl (app-text/unfurl);
1. waybackurls (net-misc/waybackurls).

## Install

If you use Pentoo: `emerge net-analyuzer/bbrecon`

Or put the shell script somewhere in your PATH (e.g. ~/bin).

## Usage

```
$ bbrecon --help
Bug Bounty Recon, version 0.0.4
Usage: bbrecon.sh -d DOMAIN [option] ...

  -h, --help     display this help and exit
  -d, --domain   specify the domain to scan (e.g. foo.bar)
  -e, --exclude  exclude domains to scan (e.g. foo.bar,bar.foo)
  -n, --nmap     run nmap on all harvested IP addresses

```

## Example

An example:

```
$ bbrecon -d valvesoftware.com -e foo.bar,bar.foo -n
```

![Example](example/example.png)

