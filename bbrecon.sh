#!/bin/bash

# Reset ${SECONDS}
SECONDS=0

print_help() {
  printf "Bug Bounty Recon, version 0.0.4\n"
  printf "Usage: %s -d DOMAIN [option] ...\n" "${0}"
  printf "\n"
  printf "  -h, --help     display this help and exit\n"
  printf "  -d, --domain   specify the domain to scan (e.g. foo.bar)\n"
  printf "  -e, --exclude  exclude domains to scan (e.g. foo.bar,bar.foo)\n"
  printf "  -n, --nmap     run nmap on all harvested IP addresses\n"
  printf "\n"
  exit 0
}

logo() {
    printf "%s     _     _                              
    | |__ | |__  _ __ ___  ___ ___  _ __  
    | '_ \| '_ \| '__/ _ \/ __/ _ \| '_ \ 
    | |_) | |_) | | |  __/ (_| (_) | | | |
    |_.__/|_.__/|_|  \___|\___\___/|_| |_|%s  v0.0.4\n\n" "${LRED}" "${NC}"
    printf "By 0bs1d1an, inspired by @nahamsec's lazyrecon\n\n\n"
}

string_to_array() {
  local IFS=','
  read -r -a EXCLUDE <<< "${1}"
}

get_args() {
  while [[ "$1" ]]; do
    case "$1" in
      -h|--help) print_help ;;
      -d|--domain) DOMAIN="${2}" ;;
      -e|--exclude)
        string_to_array "${2}"
        ;;
      -n|--nmap) NMAP=1 ;;
    esac
    shift
  done
}

get_vars() {
  # Terminal colours
  # GREEN=$'\033[0;32m'
  # BLUE=$'\033[0;34m'
  # MAGENTA=$'\033[0;35m'
  LGREEN=$'\033[1;32m'
  LRED=$'\033[1;31m'
  # YELLOW=$'\033[1;33m'
  NC=$'\033[0m'

  # Print "[ !! ]" or "[ OK ]" in colour
  FAILED="${BLUE}[${NC} ${LRED}!!${NC} ${BLUE}]${NC}"
  OK="${BLUE}[${NC} ${LGREEN}ok${NC} ${BLUE}]${NC}"

  # We will dump all in this directory
  WORKDIR=${DOMAIN}/$(date -I)

  # Tool paths and settings to edit if required
  SUBLIST3R_PATH=/usr/bin/sublist3r
  CT_PATH=/usr/share/massdns/scripts/ct.py
  MASSDNS_PATH=/usr/bin/massdns
  DNSRESOLVERS_PATH=/usr/share/dict/seclists/Miscellaneous/dns-resolvers.txt
  SUBBRUTE_PATH=/usr/share/massdns/scripts/subbrute.py
  MASSDNS_WORDLIST=/usr/share/bbrecon/clean-jhaddix-dns.txt
  HTTPROBE_PATH=/usr/bin/httprobe
  AQUATONE_PATH=/usr/bin/aquatone
  CHROMIUM_PATH=/usr/bin/chromium
  AQUATONE_THREADS=5
  WAYBACKURLS_PATH=/usr/bin/waybackurls
  UNFURL_PATH=/usr/bin/unfurl
  SUBDOMAIN_THREADS=10
  DIRSEARCH_PATH=/usr/bin/dirsearch
  DIRSEARCH_WORDLIST=/usr/share/dirsearch/db/dicc.txt
  DIRSEARCH_THREADS=50
}

# Offset to print "[ OK ]" or "[ !! ]"
expand_width() {
  expand -t "$(($(tput cols) + 4))"
}

run_sublist3r() {
  if ! "${SUBLIST3R_PATH}" -d "${DOMAIN}" -t 10 -v -o \
    "${WORKDIR}/sublist3r.txt" >/dev/null 2>&1; then
    printf "%s\n\nRunning sublist3r failed. Exiting...\n" "${FAILED}"
    exit 1
  fi
}

check_certspotter() {
  if ! curl -s 'https://certspotter.com/api/v0/certs?domain='"${DOMAIN}" | \
    jq '.[].dns_names[]' | sed 's/\"//g' | sed 's/\*\.//g' | sort -u | \
    grep "${DOMAIN}" >> "${WORKDIR}/certspotter.txt" 2>/dev/null; then
    printf "%s\n\nChecking certspotter failed. Exiting...\n" "${FAILED}"
    exit 1
  fi
}

check_crtsh() {
  if ! "${CT_PATH}" "${DOMAIN}" > "${WORKDIR}/tmp.txt" 2>/dev/null; then
    printf "%s\n\nChecking crt.sh failed. Exiting...\n" "${FAILED}"
    exit 1
  fi
  if [ -s "${WORKDIR}/tmp.txt" ]; then
    if ! "${MASSDNS_PATH}" -r "${DNSRESOLVERS_PATH}" -t A -q -o S -w \
      "${WORKDIR}/crtsh.txt" < "${WORKDIR}/tmp.txt"; then
      printf "%s\n\nChecking crt.sh failed. Exiting...\n" "${FAILED}"
      exit 1
    fi
    rm "${WORKDIR}/tmp.txt" >/dev/null 2>&1
  else
    printf "%s\n\nChecking crt.sh failed. Exiting...\n" "${FAILED}"
    exit 1
  fi
  if ! "${MASSDNS_PATH}" -r ${DNSRESOLVERS_PATH} -t A -q -o S -w \
    "${WORKDIR}/tmp.txt" < "${WORKDIR}/certspotter.txt" 2>/dev/null; then
    printf "%s\n\nChecking crt.sh failed. Exiting...\n" "${FAILED}"
    exit 1
  fi
}

run_massdns() {
  # Apparently some resolvers are sending fake record
  if ! "${SUBBRUTE_PATH}" "${MASSDNS_WORDLIST}" "${DOMAIN}" | ${MASSDNS_PATH} \
    -r ${DNSRESOLVERS_PATH} -t A -q -o S | grep -v 142.54.173.92 | \
    grep -v 23.202.231.167 | grep -v 23.217.138.108 \
    > "${WORKDIR}/mass.txt"; then
    printf "%s\n\nRunning massdns failed. Exiting...\n" "${FAILED}"
    exit 1
  fi
}

check_records() {
  {
    cat "${WORKDIR}/crtsh.txt"
    cat "${WORKDIR}/mass.txt"
    cat "${WORKDIR}/certspotter.txt"
  } >> "${WORKDIR}/tmp.txt"
  if !  awk '{print $3}' < "${WORKDIR}/tmp.txt" | sort -u | while \
    read -r LINE; do
      WILDCARD=$(grep -m 1 "${LINE}" < "${WORKDIR}/tmp.txt")
      echo "${WILDCARD}" >> "${WORKDIR}/cleantmp.txt"
    done; then
    printf "%s\n\nChecking records failed. Exiting...\n" "${FAILED}"
    exit 1
  fi
  rm "${WORKDIR}/tmp.txt" >/dev/null 2>&1
  if ! grep CNAME < "${WORKDIR}/cleantmp.txt" | sort -u | while read -r LINE; do
      HOSTREC=$(echo "$LINE" | awk '{print $1}')
      if [[ $(host "${HOSTREC}" | grep NXDOMAIN) != "" ]]; then
        echo "${LINE}" >> "${WORKDIR}/possible-ns-takeovers.txt"
      fi
    done; then
    printf "%s\n\nChecking records failed. Exiting...\n" "${FAILED}"
    exit 1
  fi
  awk '{print $1}' < "${WORKDIR}/cleantmp.txt" | while read -r LINE; do
    x="${LINE}"
    echo "${x%?}" >> "${WORKDIR}/alldomains.txt"
  done
  sleep 1
  rm "${WORKDIR}/cleantmp.txt" >/dev/null 2>&1
}

exclude_domains() {
  printf "%s\n" "${EXCLUDE[*]}" > "${WORKDIR}/excluded.txt"
  grep -vFf "${WORKDIR}/excluded.txt" "${WORKDIR}/alldomains.txt" \
    > "${WORKDIR}/alldomains2.txt"
  mv "${WORKDIR}/alldomains2.txt" "${WORKDIR}/alldomains.txt"
}

run_httprobe() {
  if ! sort -u < "${WORKDIR}/alldomains.txt" | "${HTTPROBE_PATH}" -c 50 \
    -t 3000 > "${WORKDIR}/responsive.txt" 2>/dev/null; then
    printf "%s\n\nRunning httprobe failed. Exiting...\n" "${FAILED}"
    exit 1
  fi
  if ! sed 's/\http\:\/\///g' < "${WORKDIR}/responsive.txt" | \
    sed 's/\https\:\/\///g' | sort -u | while read -r LINE; do
      PROBEURL=$(sort -u < "${WORKDIR}/responsive.txt" | grep -m 1 "${LINE}")
      echo "${PROBEURL}" >> "${WORKDIR}/tmp.txt"
    done; then
    printf "%s\n\nMangling httprobe output failed. Exiting...\n" "${FAILED}"
    exit 1
  fi
  if ! sort -u < "${WORKDIR}/tmp.txt" > "${WORKDIR}/httprobe.txt"; then
    printf "%s\n\nSorting httprobe output failed. Exiting...\n" "${FAILED}"
    exit 1
  fi
  sleep 1
  rm "${WORKDIR}/tmp.txt" >/dev/null 2>&1
}

run_aquatone() {
  if ! "${AQUATONE_PATH}" -chrome-path ${CHROMIUM_PATH} \
    -out "${WORKDIR}/aquatone" -threads ${AQUATONE_THREADS} -silent \
    < "${WORKDIR}/httprobe.txt" >/dev/null 2>&1; then
    printf "%s\n\nRunning aquatone failed. Exiting...\n" "${FAILED}"
    exit 1
  fi
}

run_waybackurls() {
  if ! [ -s "${WORKDIR}/httprobe.txt" ]; then
    printf "%s\n\nhttprobe output not found to run aquatone on. \
      Exiting...\n" "${FAILED}"
    exit 1
  else
    mkdir "${WORKDIR}/wayback"
  fi
  if ! ${WAYBACKURLS_PATH} < "${WORKDIR}/httprobe.txt" \
    > "${WORKDIR}/wayback/waybackurls.txt" 2>/dev/null; then
    printf "%s\n\nRunning waybackurls failed. Exiting...\n" "${FAILED}"
    exit 1
  fi

  sort -u < "${WORKDIR}/wayback/waybackurls.txt" | ${UNFURL_PATH} --unique \
    keys > "${WORKDIR}/wayback/paramlist.txt"

  sort -u < "${WORKDIR}/wayback/waybackurls.txt" | grep -P "\w+\.js(\?|$)" | \
    sort -u > "${WORKDIR}/wayback/jsurls.txt"

  sort -u < "${WORKDIR}/wayback/waybackurls.txt" | grep -P "\w+\.php(\?|$)" | \
    sort -u > "${WORKDIR}/wayback/phpurls.txt"

  sort -u < "${WORKDIR}/wayback/waybackurls.txt" | grep -P "\w+\.aspx(\?|$)" | \
    sort -u > "${WORKDIR}/wayback/aspxurls.txt"

  sort -u < "${WORKDIR}/wayback/waybackurls.txt" | grep -P "\w+\.jsp(\?|$)" | \
    sort -u > "${WORKDIR}/wayback/jspurls.txt"
}

run_dirsearch() {
  if ! xargs -P ${SUBDOMAIN_THREADS} -I % sh -c "${DIRSEARCH_PATH} \
    -e php,asp,aspx,jsp,html,zip,jar -w ${DIRSEARCH_WORDLIST} \
    -t ${DIRSEARCH_THREADS} -u % | grep Target" < "${WORKDIR}/httprobe.txt" \
    > "${WORKDIR}/dirsearch.txt" 2>/dev/null; then
    printf "%s\n\nRunning dirsearch failed. Exiting...\n" "${FAILED}"
    exit 1
  fi
}

run_nmap() {
  mkdir "${WORKDIR}/nmap"
  grep -r -E -o "([1-9][0-9]{0,2}[\.]){3}[0-9]{1,3}" "${WORKDIR}" | \
    cut -d : -f 2 | sort -nu > "${WORKDIR}/nmap-candidates.txt"
  if ! nmap -iL "${WORKDIR}/nmap-candidates.txt" --open --top-ports 50 -A \
    -oA "${WORKDIR}/nmap/nmap" >/dev/null 2>&1; then
    printf "%s\n\nRunning nmap failed. Exiting...\n" "${FAILED}"
    exit 1
  fi
}

main() {
  if [ -d "${WORKDIR}" ]; then
    printf "%s already exists. Exiting...\n" "${WORKDIR}"
    exit 1
  else
    mkdir -p "${WORKDIR}"
  fi
  
  printf 'Performing recon on %s%s%s' "${LGREEN}" "${DOMAIN}" "${NC}"
  if [ -n "${EXCLUDE[0]}" ]; then
    printf ', excluding %s%s%s\n\n' "${LRED}" "${EXCLUDE[*]}" "${NC}"
  else
    printf "\n\n"
  fi

  printf ' %s*%s Running sublist3r ... \t' "${LGREEN}" "${NC}" | expand_width
  if run_sublist3r ; then
    printf "%s\n" "${OK}"
  fi

  printf ' %s*%s Checking certspotter ... \t' "${LGREEN}" "${NC}" | expand_width
  if check_certspotter ; then
    printf "%s\n" "${OK}"
  fi

  printf ' %s*%s Checking crt.sh ... \t' "${LGREEN}" "${NC}" | expand_width
  if check_crtsh ; then
    printf "%s\n" "${OK}"
  fi

  printf ' %s*%s Running massdns ... \t' "${LGREEN}" "${NC}" | expand_width
  if run_massdns ; then
    printf "%s\n" "${OK}"
  fi

  printf ' %s*%s Check records ... \t' "${LGREEN}" "${NC}" | expand_width
  if check_records ; then
    printf "%s\n" "${OK}"
  fi

  if [ -n "${EXCLUDE[0]}" ]; then
    printf ' %s*%s Excluding domains ... \t' "${LGREEN}" "${NC}" | expand_width
    if exclude_domains ; then
      printf "%s\n" "${OK}"
    fi
  fi

  printf ' %s*%s Running httprobe ... \t' "${LGREEN}" "${NC}" | expand_width
  if run_httprobe ; then
    printf "%s\n" "${OK}"
  fi

  printf ' %s*%s Running aquatone ... \t' "${LGREEN}" "${NC}" | expand_width
  if run_aquatone ; then
    printf "%s\n" "${OK}"
  fi

  printf ' %s*%s Running waybackurls ... \t' "${LGREEN}" "${NC}" | expand_width
  if run_waybackurls ; then
    printf "%s\n" "${OK}"
  fi

  printf ' %s*%s Running dirsearch ... \t' "${LGREEN}" "${NC}" | expand_width
  if run_dirsearch ; then
    printf "%s\n" "${OK}"
  fi

  if [ -s "${WORKDIR}/possible-ns-takeovers.txt" ]; then
    printf "\nCheck the following domain(s) for possible %sNS takeover%s:\n" \
      "${LRED}" "${NC}"
    cat "${WORKDIR}/possible-ns-takeovers.txt"
  fi

  if [ -n "${NMAP}" ]; then
    printf ' %s*%s Running nmap ... \t' "${LGREEN}" "${NC}" | expand_width
    if run_nmap ; then
      printf "%s\n" "${OK}"
    fi
  fi

  printf "\nScan time: %s%s%s minutes and %s%s%s seconds.\n" \
    "${LGREEN}" "$((SECONDS / 60))" "${NC}" "${LGREEN}" "$((SECONDS % 60))" \
    "${NC}"
}

get_args "$@"
get_vars
logo
main
